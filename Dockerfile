# Use the official Rust image as the base image
FROM rust:1.69 as builder

# Create a new empty shell project
RUN USER=root cargo new --bin fullstackrest
WORKDIR /fullstackrest

# Copy our manifests
COPY ./Cargo.toml ./Cargo.toml
COPY ./Cargo.lock ./Cargo.lock

# This is a dummy build to get the dependencies cached
RUN cargo build --release
RUN rm src/*.rs

# Now that the dependencies are cached, copy the actual source code
COPY ./src ./src

# Build for release.
RUN rm ./target/release/deps/fullstackrest*
RUN cargo build --release

# Start of the final stage
FROM debian:bullseye-slim
ARG APP=/usr/src/app

EXPOSE 8080

ENV TZ=Etc/UTC \
    APP_USER=appuser

# Install OpenSSL and other dependencies - Update for newer Debian version
RUN apt-get update \
    && apt-get install -y ca-certificates tzdata openssl \
    && rm -rf /var/lib/apt/lists/*

# Setup the new app user
RUN groupadd $APP_USER \
    && useradd -g $APP_USER $APP_USER \
    && mkdir -p ${APP}

WORKDIR ${APP}

# Copy the binary from the builder stage
COPY --from=builder /fullstackrest/target/release/fullstackrest ${APP}/fullstackrest

# Use the app user
USER $APP_USER

CMD ["./fullstackrest"]
