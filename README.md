# A Rust REST API for User Management w/ Containerization and Auto Dev Ops CI/CD

Includes Dockerfile, .gitlab-ci.yml, and REST API functionality for user storage. Adding, updating, getting user info, and deleting users supported with PostgreSQL Database.

# Table of Contents
1. [Video Demo](#demo)
2. [Description](#description)
3. [Documentation for Creation](#Documenatation for Creation)

# Demo

See this: https://youtu.be/0G3cYXxR0Cg to view video demo. It showcases the REST API functionalities, Docker integration, and Gitlab's AutoDevOps (GitLab workflow to build and deploy site on push).

# Description

This project features a REST API for User Management. It stores the User ID, password, and email in a PostgreSQL database. Through requests such as the ones below, the database can be queried.

curl -X POST http://localhost:8080/users \
     -H "Content-Type: application/json" \
     -d '{"username": "AlexTurner", "email": "alex.turner@example.com"}'

returns {"id":30,"username":"AlexTurner","email":"alex.turner@example.com"}

Makes a new User with ID 30.

curl -X GET http://localhost:8080/users/30

Retrieve's the user's info.

curl -X PUT http://localhost:8080/users/30 \
     -H "Content-Type: application/json" \
     -d '{"username": "JamieCook", "email": "jamie.newemail@example.com"}'

Updates the user with ID 30.

curl -X DELETE http://localhost:8080/users/30

Deletes the user with ID 30

The application benefits from both a Dockerfile which caches the dependencies and builds the application, as well as an automated CI/CD pipeline via GitLab, which facilitates immediate updates and deployments with each code commit. 


# Documentation for Creation

Install cargo, rust, PostgreSQL.

Setup files with cargo new. 

Create a main.rs for general configuration, logging, http connection.
Create a route.rs to handle REST API and database requests.
Create a data.rs for custom data structs (Username, Email, User )
Create an auth.rs for identity authorization functionality.

'cargo build' and debug.

If using this project, 

git clone git@gitlab.com:dukeaiml/indiv2peterliu.git

Create a dockerfile to containerize (can use included dockerfile). Test containerization on local device.

Go to Gitlab repo and enable AutoDevIps. create a gitlab-ci.yml or use this repo's.

All set!


