#!/bin/bash

# Example commands interacting with GitLab API
curl -X POST $GITLAB_API_ENDPOINT/users \
     -H "Content-Type: application/json" \
     -d '{"username": "AlexTurner", "email": "alex.turner@example.com"}'
curl -X GET $GITLAB_API_ENDPOINT/users/1
curl -X PUT $GITLAB_API_ENDPOINT/users/2 \
     -H "Content-Type: application/json" \
     -d '{"username": "JamieCook", "email": "jamie.newemail@example.com"}'
curl -X GET $GITLAB_API_ENDPOINT/users/1
curl -X DELETE $GITLAB_API_ENDPOINT/users/1
curl -X GET $GITLAB_API_ENDPOINT/users/1
