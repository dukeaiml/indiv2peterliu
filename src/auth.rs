// shared/auth/src/lib.rs

// The MIT License (MIT)
// Copyright (c) 2021 Tsumanu

// Access token with Bearer

use std::future::Future;
use std::rc::Rc;
use actix_web::HttpMessage;
use actix_web::dev::{Payload, ServiceRequest, ServiceResponse};
use actix_web::web::Data;
use actix_web::{Error, FromRequest, HttpRequest, Result};
use crate::data;
use crate::db_actor;
use crate::db_actor::DbExecutor;
use crate::data::User;
use crate::data::AccessToken;
use futures_util::future::{ready, Ready};
use crate::db_actor::Handle;


use crate::middleware::*;

#[macro_export]
macro_rules! require_user {
    ($id: ident) => {{
        match $id.user().await {
            None => {
                log::warn!("Terminating authorized access");
                return HttpResponse::Unauthorized().body("");
            }
            Some(user) => user,
        }
    }};
}

struct IdentityItem {
    access_token: Option<data::AccessToken>,
    database: Data<db_actor::DbExecutor>,
}

impl IdentityItem {
    pub fn new(
        access_token: Option<data::AccessToken>,
        database: Data<db_actor::DbExecutor>,
    ) -> Self {
        Self {
            database,
            access_token,
        }
    }
}

#[derive(Debug, Clone)]
enum UserState {
    NoInitialized,
    Anonymouse,
    User(Rc<data::User>),
}

#[derive(Clone, Debug)]
pub struct Identity {
    database: Data<db_actor::DbExecutor>,
    access_token: Option<AccessToken>,
    user: UserState,
}

impl Identity {
    pub fn new(access_token: Option<AccessToken>, database: Data<DbExecutor>) -> Self {
        Self {
            database,
            access_token,
            user: UserState::NoInitialized,
        }
    }

    pub fn access_token(&self) -> Option<&AccessToken> {
        self.access_token.as_ref()
    }

    pub async fn user(&mut self) -> Option<Rc<User>> {
        if let Some(u) = self.try_user_ref() {
            return u;
        }
        let token = self.access_token()?.clone();
        self.load_user(token).await;
        self.user_ref()
    }

    fn try_user_ref(&self) -> Option<Option<Rc<User>>> {
        match &self.user {
            UserState::Anonymouse => Some(None),
            UserState::User(user) => Some(Some(user.clone())),
            _ => None,
        }
    }

    fn user_ref(&self) -> Option<Rc<User>> {
        match &self.user {
            UserState::User(user) => Some(user.clone()),
            _ => None,
        }
    }

    async fn load_user(&mut self, token: AccessToken) {
        self.user = self
            .database
            .handle(db_actor::AuthorizeUser { token })
            .await
            .ok()
            .map(Rc::new)
            .map_or_else(|| UserState::Anonymouse, UserState::User);
        log::info!("CURRENT USER IS {:?}", self.user);
    }
}

impl FromRequest for Identity {
    type Error = Error;
    type Future = Ready<Result<Identity, Error>>;

    #[inline]
    fn from_request(req: &HttpRequest, _: &mut Payload) -> Self::Future {
        let id = req
            .extensions()
            .get::<IdentityItem>()
            .map(|id| Identity::new(id.access_token.clone(), id.database.clone()))
            .expect("Identity service unavailable");
        ready(Ok(id))
    }
}

pub trait IdentityPolicy: Sized + 'static {
    type Future: Future<Output = Result<Option<String>, Error>>;

    type ResponseFuture: Future<Output = Result<(), Error>>;

    fn from_request(&self, req: &mut ServiceRequest) -> Self::Future;

    fn to_response<B>(
        &self,
        identity: Option<String>,
        changed: bool,
        response: &mut ServiceResponse<B>,
    ) -> Self::ResponseFuture;
}

pub trait RequestIdentity {
    fn get_identity(&self) -> Option<String>;
}
