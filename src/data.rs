use serde::{Serialize, Deserialize};
use sqlx::Type; // This import is correct

#[derive(Type, Debug, Serialize, Deserialize, Clone)]
#[sqlx(type_name = "INT4", transparent)]
pub struct UserId(pub i32);

#[derive(Type, Debug, Serialize, Deserialize, Clone)]
#[sqlx(type_name = "UUID", transparent)]
pub struct ShareToken(pub uuid::Uuid);

#[derive(Type, Debug, Serialize, Deserialize, Clone)]
#[sqlx(type_name = "UUID", transparent)]
pub struct AccessToken(pub uuid::Uuid);

#[derive(Type, Debug, Serialize, Deserialize, Clone)]
#[sqlx(type_name = "VARCHAR", transparent)]
pub struct Username(pub String);

#[derive(Type, Debug, Serialize, Deserialize, Clone)]
#[sqlx(type_name = "VARCHAR", transparent)]
pub struct Email(pub String);

#[derive(sqlx::FromRow, Debug, Serialize, Deserialize, Clone)]
pub struct User {
    pub id: UserId,
    pub username: Username,
    pub email: Email,
}
