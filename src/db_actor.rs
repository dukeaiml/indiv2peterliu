// actors/src/lib.rs

use async_trait::async_trait;
use crate::data;
use sqlx::Error as SqlxError; // Import the SQLx error type

pub type DbExecutor = sqlx::PgPool;

#[async_trait]
pub trait Handle<Msg> {
    type Result;

    // Specify the error type as sqlx::Error for the Result
    async fn handle(&self, msg: Msg) -> std::result::Result<Self::Result, SqlxError>;
}

pub struct AuthorizeUser {
    pub token: data::AccessToken,
}

#[async_trait]
impl Handle<AuthorizeUser> for sqlx::PgPool {
    type Result = data::User;

    // Correct the Result type to include SqlxError as the error type
    async fn handle(&self, msg: AuthorizeUser) -> std::result::Result<Self::Result, SqlxError> {
        let user = sqlx::query_as::<_, data::User>(
            r#"
            SELECT users.id, users.email, users.username
            FROM users
            INNER JOIN tokens ON users.id = tokens.user_id
            WHERE tokens.access_token = $1
            "#
        )
        .bind(msg.token)
        .fetch_one(self)
        .await?;
        Ok(user)
    }
}

pub struct AllUsers {}

#[async_trait]
impl Handle<AllUsers> for DbExecutor {
    type Result = Vec<data::User>;

    // Correct the Result type to include SqlxError as the error type
    async fn handle(&self, _msg: AllUsers) -> std::result::Result<Self::Result, SqlxError> {
        let users = sqlx::query_as::<_, data::User>(
            r#"
            SELECT id, email, username
            FROM users
            "#
        )
        .fetch_all(self)
        .await?;
        Ok(users)
    }
}
