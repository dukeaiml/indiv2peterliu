// src/main.rs

use actix_web::{web, App, HttpServer, middleware};
use sqlx::postgres::PgPoolOptions;
use std::env;
use env_logger::{Env, Builder};

mod route;
mod data;
pub mod auth;
mod db_actor;


use crate::middleware::*;

#[tokio::main]
async fn main() -> std::io::Result<()> {
    dotenv::dotenv().ok(); // Load .env file if exists
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init(); // Initialize logger with custom format

    let database_url = env::var("DATABASE_URL").expect("DATABASE + URL must be set");
    let db_pool = PgPoolOptions::new().connect(&database_url).await.expect("Failed to create pool.");

    HttpServer::new(move || {
        App::new()
            .wrap(middleware::Logger::new("%a \"%r\" %s %b \"%{User-Agent}i\" %{Referer}i %{X-Forwarded-For}i %{X-Request-Id}i %{X-Request-Id}i %{X-Request-Id}i"))
            .app_data(web::Data::new(db_pool.clone()))
            .configure(route::config) // Make sure to use .configure to include routes
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}