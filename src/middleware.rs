// shared/auth/src/middleware.rs

// The MIT License (MIT)
// Copyright (c) 2021 Tsumanu

use std::error::Error as StdError;
use std::rc::Rc;

use actix_web::body::{AnyBody, MessageBody};
use actix_web::dev::{Service, ServiceRequest, ServiceResponse, Transform};
use actix_web::web::Data;
use actix_web::{Error, HttpMessage, Result};
use futures_util::future::{ready, FutureExt as _FE, LocalBoxFuture, Ready, TryFutureExt as TFE_};

use super::*;

static AUTHORIZATION: &str = "Authorization";
static BEARER: &str = "Bearer ";
static TOKEN_LEN: usize = 36;

#[derive(Default, Debug)]
pub struct IdentityService;

impl<S, B> Transform<S, ServiceRequest> for IdentityService
where
    S: Service<ServiceRequest, Response = ServiceResponse<B>, Error = Error> + 'static,
    S::Future: 'static,
    B: MessageBody + 'static,
    B::Error: StdError,
{
    type Response = ServiceResponse;
    type Error = Error;
    type Transform = IdentityServiceMiddleware<S>;
    type InitError = ();
    type Future = Ready<Result<Self::Transform, Self::InitError>>;

    fn new_transform(&self, service: S) -> Self::Future {
        ready(Ok(IdentityServiceMiddleware {
            service: Rc::new(service),
        }))
    }
}

pub struct IdentityServiceMiddleware<S> {
    pub(crate) service: Rc<S>,
}

impl<S> Clone for IdentityServiceMiddleware<S> {
    fn clone(&self) -> Self {
        Self {
            service: Rc::clone(&self.service),
        }
    }
}

impl<S, B> Service<ServiceRequest> for IdentityServiceMiddleware<S>
where
    S: Service<ServiceRequest, Response = ServiceResponse<B>, Error = Error> + 'static,
    S::Future: 'static,
    B: MessageBody + 'static,
    B::Error: StdError,
{
    type Response = ServiceResponse;
    type Error = Error;
    type Future = LocalBoxFuture<'static, Result<Self::Response, Self::Error>>;

    actix_service::forward_ready!(service);

    fn call(&self, req: ServiceRequest) -> Self::Future {
        let srv = Rc::clone(&self.service);
        async move {
            let database = req.app_data::<Data<db_actor::DbExecutor>>();
            let token = req
                .headers()
                .get(AUTHORIZATION)
                .and_then(|s| s.to_str().ok())
                .filter(|s| {
                    log::info!("Bearer header {:?}", s);
                    s.contains(BEARER)
                })
                .filter(|s| s.len() == BEARER.len() + TOKEN_LEN)
                .and_then(|s| {
                    log::info!("Only token {:?}", &s[BEARER.len()..]);
                    uuid::Uuid::parse_str(&s[BEARER.len()..]).ok()
                })
                .map(data::AccessToken::new);
            log::info!("token {:?}", token);
            req.extensions_mut()
                .insert(IdentityItem::new(token, database.unwrap().clone()));

            Ok(srv
                .call(req)
                .await?
                .map_body(|_, body| AnyBody::from_message(body)))
        }
        .map_ok(|res| res.map_body(|_, body| AnyBody::from_message(body)))
        .boxed_local()
    }
}
