use actix_web::{web, HttpResponse, Responder};
use actix_web::web::{ServiceConfig, Json};
use serde::{Serialize, Deserialize};
use crate::db_actor::DbExecutor;
use crate::data::{User, UserId, Email, Username};
use sqlx::Row; // Required for getting rows

#[derive(Serialize, Deserialize)]
struct CreateUserRequest {
    username: String,
    email: String,
}

async fn create_user(db: web::Data<DbExecutor>, body: Json<CreateUserRequest>) -> impl Responder {
    let result = sqlx::query(
        "INSERT INTO users (username, email) VALUES ($1, $2) RETURNING id, username, email"
    )
    .bind(&body.username)
    .bind(&body.email)
    .fetch_one(db.get_ref())
    .await;

    match result {
        Ok(row) => {
            let user_id: i32 = row.get("id"); // Directly get the i32 value
            let user = User {
                id: UserId(user_id), // Wrap the i32 value with UserId
                username: Username(row.get("username")),
                email: Email(row.get("email")),
            };
            HttpResponse::Created().json(user)
        },
        Err(e) => {
            eprintln!("Failed to insert user: {:?}", e);
            HttpResponse::InternalServerError().finish()
        }
    }
}

async fn update_user(db: web::Data<DbExecutor>, user_id: web::Path<i32>, body: Json<CreateUserRequest>) -> impl Responder {
    let result = sqlx::query(
        "UPDATE users SET username = $1, email = $2 WHERE id = $3 RETURNING id, username, email"
    )
    .bind(&body.username)
    .bind(&body.email)
    .bind(user_id.into_inner())
    .fetch_one(db.get_ref())
    .await;

    match result {
        Ok(row) => {
            let user_id: i32 = row.get("id"); // Directly get the i32 value
            let user = User {
                id: UserId(user_id), // Wrap the i32 value with UserId
                username: Username(row.get("username")),
                email: Email(row.get("email")),
            };
            HttpResponse::Ok().json(user)
        },
        Err(_) => HttpResponse::InternalServerError().finish(),
    }
}


async fn get_user(db: web::Data<DbExecutor>, user_id: web::Path<i32>) -> impl Responder {
    let user_id = user_id.into_inner(); // Extract the user ID from the path

    let result = sqlx::query_as::<_, User>(
        "SELECT id, username, email FROM users WHERE id = $1"
    )
    .bind(user_id) // Bind the user ID
    .fetch_optional(db.get_ref()) // Use fetch_optional to handle the case when user is not found
    .await;

    match result {
        Ok(Some(user)) => {
            println!("User found: {:?}", user);
            HttpResponse::Ok().json(user)
        },
        Ok(None) => {
            println!("User not found");
            HttpResponse::NotFound().json("User not found")
        },
        Err(e) => {
            eprintln!("Failed to fetch user: {:?}", e);
            HttpResponse::InternalServerError().finish()
        }
    }
}



async fn delete_user(db: web::Data<DbExecutor>, user_id: web::Path<i32>) -> impl Responder {
    let result = sqlx::query("DELETE FROM users WHERE id = $1")
        .bind(user_id.into_inner())
        .execute(db.get_ref())
        .await;

    match result {
        Ok(_) => HttpResponse::NoContent().finish(),
        Err(_) => HttpResponse::InternalServerError().finish(),
    }
}

pub fn config(cfg: &mut ServiceConfig) {
    cfg.service(
        web::scope("/users")
            .route("", web::post().to(create_user))
            .route("/{user_id}", web::get().to(get_user))
            .route("/{user_id}", web::put().to(update_user))
            .route("/{user_id}", web::delete().to(delete_user)),
    );
}
